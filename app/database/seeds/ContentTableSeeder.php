<?php

use Fletch\Entities\Content as Content;

class ContentTableSeeder extends Seeder {

	public function run() {

		$faker = Faker\Factory::create();

		// Create a couple more generic pages
		foreach(range(1, 9) as $index) {
			Content::create([
					'element_id' => $index,
					'data' => '<p>' . $faker->sentence($faker->numberBetween(15, 135)) . '</p>'
				]);			
		}


	}
}
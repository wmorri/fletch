<?php namespace Fletch\Elements;

class BaseElement {

	protected  $auth;
	protected  $element;
	protected  $deleted;
	protected  $contentClass;

	public function __construct($element, $auth = false, $contentClass = 'Fletch/Elements/Content') {
		$this->element = $element;
		$this->auth = $auth;
		$this->contentClass = $contentClass;
		$this->deleted = (isset($this->element->drafts->deleted) && $this->element->drafts->deleted == '1') ? true : false;
	}

	/**
	 * Get the correct element region
	 *
	 * The element's region may be different depending on whether
	 * or not the user is logged in, since guests only see published content.
	 *
	 * @return  string  The element's region on the page 
	 */
	public function getRegion()
	{
		return ($this->auth && isset($this->element->drafts)) ? $this->element->drafts->region : $this->element->region;
	}

	/**
	 * Get the correct element position
	 *
	 * The element's position may be different depending on whether
	 * or not the user is logged in, since guests only see published content.
	 *
	 * @return  string  The element's position on the page 
	 */
	public function getPosition()
	{
		return ($this->auth && isset($this->element->drafts)) ? $this->element->drafts->position : $this->element->position;
	}

	/**
	 * Prepare the data based on whether or not the user is logged in.
	 *
	 * When a guest is viewing the page, only published content will be shown,
	 * which also means that deleted items are atill shown. On the other hand,
	 * when an admin is logged in an element will show drafted content,
	 * which also means that deleted items are hidden.
	 *
	 * @param   boolean   $auth   Whether or not the user is authenticated
	 * @return  array
	 */
	public function getContent()
	{
		$allContent = array();

		// If the user is authenticated
		if($this->auth) {
			foreach($this->element->content as $key => $content) {
				$allContent[$key] = new \Fletch\Elements\Content();
				$allContent[$key]->data = (isset($content->drafts->data)) ? $content->drafts->data : $content->data;
			}
		}
		// If the user is a guest
		else {
			foreach($this->element->content as $key => $content) {
				$allContent[$key] = new \Fletch\Elements\Content();
				$allContent[$key]->data = $content->data;
			}
		}

		return $allContent;
	}

}
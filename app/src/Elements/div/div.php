<?php namespace Fletch\Elements\Div;

class Div extends BaseElement implements Fletch\Elements\ElementInterface {

	/** @var  string  The primary stylesheet */
	public $stylesheet = './assets/style.css';

	/** @var  string  The class to be added to all instances of the element*/
	public $class = 'fletch-div';

	/** @var  string  The primary javascript file*/
	public $script = './assets/javascript.js';

	/** @var  string  The main identifier for this element type*/
	public $slug = 'div';

	/** @var  array  The content rows (and defaults) for this element*/
	public $content = array(
		'<p>No text has been entered.</p>'
		);

	/** @var  boolean  Whether or not the element will appear under 'Add Items' */
	public $hidden = false;

	/**
	 * Render a div element
	 *
	 * @param  array  $content  all of the content neccessary for rendering
	 */
	public function render(array $content) {
        
        $element_html = View::make('elements.'.$new_element->type, array('element' => $new_element, 'content' => $all_content, 'page' => $page));
        $finished_html = View::make('elements._wrapper', array('element_id' => $new_element->id, 'element_html' => $element_html, 'classes' => $classes));
	
	}
}
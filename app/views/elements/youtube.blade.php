{{-- View for the YOUTUBE element --}}
{{-- This view is used for all YOUTUBE elements within the Fletch CMS --}}

{{-- $content[0]->data   is the youtube code --}}
{{-- $content[0]->id     is the id of the youtube code --}}
{{-- $element            contains all element data including primarily ->id --}}

@if (Auth::check() === true)

	<div class="video-container">
		<object width="420" height="315">
			<param name="movie" value="http://www.youtube.com/v/{{ $content[0]->data }}?hl=en_US&amp;version=3&amp;rel=0&amp;showinfo=0&amp;autohide=1"></param>
			<param name="allowFullScreen" value="true"></param>
			<param name="allowscriptaccess" value="always"></param>
			<param name="wmode" value="opaque">
			<embed wmode="opaque" src="http://www.youtube.com/v/{{ $content[0]->data }}?hl=en_US&amp;version=3&amp;rel=0&amp;showinfo=0&amp;autohide=1" type="application/x-shockwave-flash" width="420" height="315" allowscriptaccess="always" allowfullscreen="true"></embed>
		</object>
	</div>

@else

	<div class="video-container">
		<iframe width="420" height="315" src="//www.youtube.com/embed/{{ $content[0]->data }}?rel=0&amp;showinfo=0&amp;autohide=1" frameborder="0" allowfullscreen></iframe>
	</div>

@endif

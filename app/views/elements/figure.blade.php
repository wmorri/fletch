{{-- View for the FIGURE element --}}
{{-- This view is used for all FIGURE elements within the Fletch CMS --}}

{{-- $content[0]->data   is the url of the image --}}
{{-- $content[0]->id     is the database id of the URL row --}}
{{-- $content[1]->data   is the text for the caption --}}
{{-- $content[1]->id     is the database id for the caption text --}}
{{-- $element            contains all element data including primarily ->id --}}

<figure>        
    <img src="{{ URL::to('upload/img') }}/{{ $content[0]->data }}" id="fletching_img_{{ $element->id }}" class="editableImage" data-url-id="{{ $content[0]->id }}" />
        
	@if (Auth::check() === true)
		{{-- Make the caption editable if the user is logged in --}}
		<p id="fletching_{{ $element->id }}" contenteditable="true" class="fl-editable" data-content-id="{{ $content[1]->id }}">
	@else
		{{-- Otherwise, the caption should not be editable --}}
		<p id="fletching_{{ $element->id }}">
	@endif
		{{-- Display the textual content inside the caption --}}
	    {{ $content[1]->data }}
	</p>
</figure>
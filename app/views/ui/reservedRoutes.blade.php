<script>
	// Store used routes, to notify the user immediately if they
	// are attempting to create a page with a route that is already in use
	var reserved_routes = [@foreach($reserved_routes as $route)'{{ $route }}', @endforeach''];
</script>
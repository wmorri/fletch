<div id="dropdown-1" class="fl-dropdown regular fl-dropdown-tip">
	<ul class="fl-dropdown-menu">
	  <li><a id="settings_manage_current_page" class='icon-cog' href="#">Manage Current Page</a></li>
	  <li><a id="settings_manage_social_media" class="icon-facebook" href="#">Manage Social Media</a></li>
	  <li><a id="settings_manage_global_settings" class='icon-cogs' href="#">Manage Site Settings</a></li>
	  <li><a id="settings_create_new_page" class='icon-doc-new' href="#">Create a New Page</a></li>
	</ul>
</div>
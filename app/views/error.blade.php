@extends('layouts.default')

@section('title')
    Page Not Found
@endsection

@section('content')

	<div class="error-page">
	    <h1>Page Not Found</h1>
	    <p>Sorry, but the page you were trying to view does not exist.</p>		
	</div>



@endsection
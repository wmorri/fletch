<?php

use Fletch\Entities\User as User;
use Fletch\Entities\Element as Element;
use Fletch\Entities\Content as Content;
use Fletch\Entities\ContentDraft as ContentDraft;
use Fletch\Entities\ElementDraft as ElementDraft;
use Fletch\Entities\Setting as Setting;
use Fletch\Entities\Page as Page;

class UserController extends BaseController {

	// all of these actions should only be available if logged in

	public $restful = true;

	public function get_loginui() {
		// Display the log in page
		return View::make('login.index');
	}

	public function post_login() {
				
		// get POSTed credentials
	    $username = Input::get('username');
	    $password = Input::get('password');
	    $redirect_url = Input::get('redirect_url');

	    $credentials = array('username' => $username, 'password' => $password);

	    // use Auth to log the user in
	    if (Auth::attempt($credentials) )
	    {
	        // We are now logged in
	        return Redirect::to($redirect_url);
	    }
	    else
	    {
	        // Auth failure! lets go back to the login
	        // and pass any error notification we want
	        return Redirect::to('login')
	            ->with('login_errors', true)
	            ->with('redirect_url', $redirect_url);

	    }

	}

	public function get_logout() {
		// Log the user out
		Auth::logout();
		return Redirect::back();
	}

}

?>
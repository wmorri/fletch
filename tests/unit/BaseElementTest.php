<?php
use \Fletch\Elements\BaseElement as BaseElement;

class BaseElementTest extends \PHPUnit_Framework_TestCase
{

    protected $eloquentElement;

    protected function setUp()
    {
        $eloquentElement                          = new stdClass();
        $eloquentElement->region                  = '1';
        $eloquentElement->position                = '1';
        $eloquentElement->drafts                  = new stdClass();
        $eloquentElement->drafts->region          = '3';
        $eloquentElement->drafts->position        = '3';
        $eloquentElement->content                 = array();
        $eloquentElement->content[0]              = new stdClass();
        $eloquentElement->content[0]->data        = 'Hello World';
        $eloquentElement->content[0]->drafts       = new stdClass();
        $eloquentElement->content[0]->drafts->data = 'Goodbye World';
        $eloquentElement->content[1]              = new stdClass();
        $eloquentElement->content[1]->data        = 'Hello Earth';


        $this->eloquentElement = $eloquentElement;
    }

    protected function tearDown()
    {
    }

    /**
     * Test that we can return the region of an element for an authenticated user
     */
    public function testAuthenticatedRegionRetrieval()
    {
        // Create the BaseElement
        $myElement = new BaseElement($this->eloquentElement, true);

        $this->assertEquals('3', $myElement->getRegion());
    }

    /**
     * Test that we can return the region of an element for a guest user
     */
    public function testGuestRegionRetrieval()
    {
        // Create the BaseElement
        $myElement = new BaseElement($this->eloquentElement, false);

        $this->assertEquals('1', $myElement->getRegion());
    }

    /**
     * Test that we can return the region of an element for an authenticated user
     */
    public function testAuthenticatedPositionRetrieval()
    {
        // Create the BaseElement
        $myElement = new BaseElement($this->eloquentElement, true);

        $this->assertEquals('3', $myElement->getPosition());
    }

    /**
     * Test that we can return the region of an element for a guest user
     */
    public function testGuestPositionRetrieval()
    {
        // Create the BaseElement
        $myElement = new BaseElement($this->eloquentElement, false);

        $this->assertEquals('1', $myElement->getPosition());
    }

    /**
     * Test that we can get the correct content for a guest user
     */
    public function testGuestContentRetrieval()
    {
        // Create the BaseElement
        $myElement = new BaseElement($this->eloquentElement, false, 'stdClass');
        $content = $myElement->getContent();

        $this->assertCount(2, $content);
        $this->assertEquals('Hello World', $content[0]->data);
        $this->assertEquals('Hello Earth', $content[1]->data);
    }

    /**
     * Test that we can get the correct content for an admin user
     */
    public function testAdminContentRetrieval()
    {
        // Create the BaseElement
        $myElement = new BaseElement($this->eloquentElement, true, 'stdClass');
        $content = $myElement->getContent();

        $this->assertCount(2, $content);
        $this->assertEquals('Goodbye World', $content[0]->data);
        $this->assertEquals('Hello Earth', $content[1]->data);
    }

}
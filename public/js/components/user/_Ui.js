/*!
 * User Interface Module
 * Written by Pete Molinero of Laterna Studio
 * http://www.laternastudio.com
 * All Rights Reserved.
 *
 * Synopsis:
 * The user interface for Fletch. Includes elements such as the top bar,
 * the element controls, and the settings widget. The Element Creator is
 * in a separate module, for now.
 */


var Ui = {

	notificationArea : $('#fl-notification'),
	publishUndoArea : $('#fl-publishing'),
	elementControls : $('div.fl-element-controls'),

	// AJAX Publishing is currently disabled
	// publishButton : $('#publish_drafts'),
	// undoButton : $('#undo_drafts'),

	// Initialize anything that needs to be initialized
	init: function() {
		this.listen();
	},

	// Begin listening on some of the UI elements
	listen: function() {

		// AJAX Publishing is currently disabled
		// *************************************
		// // Listen on the publish button
		// this.publishButton.on('click', function() {
		// 	Ui.publish();
		// });

		// // Listen on the undo button
		// this.undoButton.on('click', function() {
		// 	Ui.undo();
		// });

		// Listen on the LI, and display the element controls
		// when there is a mouseover.
		$('.fl-region-list').on(
		{
			mouseenter: function() 
			{	
				// Don't show the element controls if the user is currently
				// dragging content, or if the editor is currently open
				if(!Reposition.dragging && !CKEditorAJAX.editor_open) {
					$(this).children('div.fl-element-controls').show();
				}
			},
			mouseleave: function()
			{
				$(this).children('div.fl-element-controls').hide();
			}
		// Listen on all LI's within .fl-region-list
		}, 'li');


		// Listen on the drag icon, and turn on "draggable" when it is moused over
		// This is because being "draggable" disable's the CKEDITOR (if I remember correctly)
		$(document).on(
		{
			mouseenter: function() 
			{
				$('.fl-region-list>li').attr('draggable', 'true');
			},
			mouseleave: function()
			{
				$('.fl-region-list>li').attr('draggable', 'false');
			}
		}, 'span.move');


		// Listen on the trash icon--just a click listen though.
		$(document).on(
		{
			click: function() 
			{
				// Grab element id (database id, that is)
				var id = $(this).closest('li').attr('data-element-id');

				// Show a confirmation box to prevent accidental deletions
				Utility.confirm(
					"Are you sure you'd like to delete this element?",
					'Confirm Delete Request',
					'icon-attention',
					function() {
						Database.delete_element(id);
				});
			}
		}, 'span.delete');

		// Listen on the edit icon--just a click listen though.
		$(document).on(
		{
			click: function() 
			{
				// Grab element id (database id, that is)
				var element_id = $(this).closest('li').attr('data-element-id');

				var html_element = $(this).closest('li').find(".option");
				var content_id = html_element.attr('data-content-id');
				var heading = html_element.attr('data-heading');
				var message = html_element.attr('data-message');
				var placeholder = html_element.attr('data-placeholder');
				var button = html_element.attr('data-button-text');
				var icon = html_element.attr('data-icon');
				var value = html_element.val();

				// Show a confirmation box to prevent accidental deletions
				Utility.entryModal(
					message,
					heading,
					icon,
					placeholder,
					button,
					value,
					function(data) {

						// Save the new caption
						Database.update_content(
							element_id,
							'text',
							{
								content_id: content_id,
								contents: data
							},
							// Success function for the database update
							function(data) { 
								var input = $('input[data-content-id="' + data.content_id + '"]');

								// Update the hidden input value
								input.val(data.contents).trigger('change');
							});
					}
				);
			}
		}, 'span.edit');


		// Listen on the "Manage Current Page" menu item
		$('#settings_manage_current_page').on('click', function(e) {
			e.preventDefault();
			Ui.showSettings('manage_current_page');
		});

		// Listen on the "Manage Social Media" menu item
		$('#settings_manage_social_media').on('click', function(e) {
			e.preventDefault();
			Ui.showSettings('manage_social_media');
		});

		// Listen on the "Manage Image Slideshow" menu item
		$('#settings_manage_image_slideshow').on('click', function(e) {
			e.preventDefault();
			Ui.showSettings('manage_image_slideshow');
		});

		// Listen on the "Manage Site Settings" menu item
		$('#settings_manage_global_settings').on('click', function(e) {
			e.preventDefault();
			Ui.showSettings('manage_global_settings');
		});

		// Listen on the "Create New Page" menu item
		$('#settings_create_new_page').on('click', function(e) {
			e.preventDefault();
			Ui.showSettings('create_new_page');
		});

		// Listen on the "Cancel" button on the settings panel
		$('#settings_cancel').on('click', function() {
			Ui.hideSettings();
		});

		// Listen on the "Save" button on the settings panel
		$('#settings_save').on('click', function(e) {
			Ui.saveSettings(e);
		});
	},

	// Display the publish and undo buttons
	showPublishPanel:  function() {
		target = this.publishUndoArea;
		target.show();
	},

	// Hide the publish and undo buttons
	hidePublishPanel:  function() {
		target = this.publishUndoArea;
		target.hide();
	},

	// Hide the move/delete element controls
	hideElementControls: function() {
		this.elementControls.hide();
	},

	// Set a regular black notification message on the top bar
	setNotificationMessage: function(text) {
		target = this.notificationArea;
		target.html(text);
		target.show();
	},

	// Perform the publish action
	publish: function() {
		var that = this;

		$.ajax({
			type: 'POST',
			url: '/elements',
			data: { _method: 'PUT' },
			beforeSend: function() {},
			success: function(data) {
				if(data.success == 'false') {
					that.setNotificationMessage('<span style="color: red;">' + data.message + '</span>');
				}
				else {
					that.hidePublishPanel();
					that.setNotificationMessage(data.message);
				}
			},
			dataType: 'json'
			});
	},

	// Perform the undo action
	undo: function() {
		var that = this;
		
		$.ajax({
			type: 'POST',
			url: '/elements',
			data: { _method: 'DELETE' },
			beforeSend: function() {},
			success: function(data) {
				if(data.success == 'false') {
					that.setNotificationMessage('<span style="color: red;">' + data.message + '</span>');
				}
				else {
					location.reload();
				}
			},
			dataType: 'json'
			});
	},

	// Begin listening on the publish button
	activatePublish: function(obj) {
		var that = this;

		obj.on('click', function() {
			that.publish();
		});
	},

	// Begin listening on the undo button
	activateUndo: function(obj) {
		var that = this;
		obj.on('click', function() {
			that.undo();
		});		
	},

	// Display a red alert message at the top of the settings panel
	showSettingAlert: function(message) {
		var panel = $('#settings_panel');
		var new_height = (panel.height() + 48) + 'px';
		panel.css('height', new_height);
		var title = $('#settings_panel #alert');
		title.html(message);		
	},

	// Remove the alert message from the top of the settings panel
	clearSettingAlert: function() {
		var title = $('#settings_panel #alert');

		if(title.html() !== '') {
			var panel = $('#settings_panel');
			panel.css('height', (panel.height() - 48) + 'px');
			title.html('');			
		}
	},

	// Display the settings panel
	showSettings: function(type) {
		var panel = $('#settings_panel');
		var title = $('#settings_panel #title');
		var content = $('#settings_panel #content');
		var form = $('#settings_form');
		var method = $('#settings_form_method');

		// Record the type, for validation
		panel.attr('data-type', type);

		// Clear previous settings
		title.html('');
		content.html('');

		var content_row_one,
			content_row_two,
			content_row_three,
			content_row_four,
			layout,
			divider;
	
		switch(type) {

			case 'manage_image_slideshow' :
				// Set the settings panel title
				title.text('Manage Image Slideshow');

				// Set the form action and method
				form.attr('action', '/settings?type=slideshow');
				method.attr('value', 'POST');

				// Pull up the page info
				var slides = $('.slides').html();
				var rows = Math.ceil($('ul.slides > li').length / 5);
				//alert(rows);
				var height = 24.35 + (rows * 6.25);

				panel.css('height', height + 'em');

				// This is row
				content_row_one = '<div class="twelvecol last image-slideshow-list clearfix" style="margin-top: 1em;">';
				content_row_one    +=     '<ul>';	
				content_row_one    +=         slides;				
				content_row_one    +=     '</ul>';
				content_row_one    += '</div>';

				// This is row
				content_row_two = '<div class="twelvecol last image-slideshow-upload" style="margin-top: 1em;">';
				content_row_two    +=     '<fieldset>';
				content_row_two    +=       '<legend>Add a New Image</legend>';	
				content_row_two    +=       '<input type="file" name="image" id="image" />';	
				content_row_two    +=     '</fieldset>';	
				content_row_two    += '</div>';


				// Display all three rows
				content.html(content_row_one + content_row_two);
				content.find('li').each(function() {

					// Remove flexslider styling
					$(this).attr('style', '');

					// Change to thumbnails
					var src = $(this).find('img').attr('src');
					src = src.substring(0, src.length - 4) + '_thumb.jpg';
					$(this).find('img').attr('src', src);

					// Build delete checkboxes
					var setting_id = $(this).attr('data-setting-id');
					$(this).append('<div class="delete-control"><input type="checkbox" name="delete_' + setting_id + '" /> Delete</div>');
				});


			break;

			case 'manage_global_settings' :
				// Set the settings panel title
				title.text('Manage Site Settings');
				panel.css('height', '30em');

				// Set the form action and method
				form.attr('action', '/settings?type=global');
				method.attr('value', 'PUT');

				// Pull up the page info
				var emaillist = $('#setting_emaillist').text();
				var contact_form_email = $('#setting_contact_form_email').text();

				// Build the panel content
				// This is row one
				content_row_one = '<div class="fourcol">';
				content_row_one    +=     '<fieldset class="change-password">';
				content_row_one    +=       '<legend>Change Password</legend>';
				content_row_one    +=       '<label class="fl-label" for="old_password">Old Password</label>';
				content_row_one    +=       '<input type="password" class="fl-password" id="old_password" name="old_password"  placeholder="Enter a title..." />';
				content_row_one    +=       '<label class="fl-label" for="new_password">New Password</label>';
				content_row_one    +=       '<input type="password" class="fl-password" id="new_password" name="new_password"  placeholder="Enter a title..." />';
				content_row_one    +=       '<label class="fl-label" for="confirm_new_password">Confirm New Password</label>';
				content_row_one    +=       '<input type="password" class="fl-password" id="confirm_new_password" name="confirm_new_password"  placeholder="Enter a title..." />';
				content_row_one    +=   '</fieldset>';
				content_row_one    += '</div>';

				// This is row two
				content_row_two = '<div class="fourcol">';
				content_row_two    +=     '<label class="fl-label" for="emaillist">E-mail List (<a href="#" id="unlock_emaillist">Unlock</a>)</label>';
				content_row_two    +=     '<textarea class="fl-textarea" name="emaillist" id="emaillist" disabled="disabled" placeholder="No e-mails yet...">' + emaillist + '</textarea>';
				content_row_two    += '</div>';

				// This is row three
				content_row_three = '<div class="fourcol last">';
				content_row_three    +=     '<label class="fl-label" for="contact_form_email">Contact Form E-mail <abbr class="icon-info-circled" title="This is the e-mail address that contact form messages will be sent to."></abbr></label>';
				content_row_three    +=     '<input type="text" class="fl-text" id="contact_form_email" name="contact_form_email"  value="' + contact_form_email + '" placeholder="Enter an e-mail..." />';
				content_row_three    += '</div>';

				// Display all three rows
				content.html(content_row_one + content_row_two + content_row_three);

				// Activate the unlock button
				$('#unlock_emaillist').on('click', function(e) {
					e.preventDefault();
					$('#emaillist').attr('disabled', false);
				});

				// Select the appropriate layout
				layout = $('#global_page_layout').text();
				$('.fl-layouts > input#' + layout).attr("checked","checked");

			break;

			case 'manage_current_page' :
				// Set the settings panel title
				title.text('Manage Current Page');
				panel.css('height', '37em');

				// Set the form action and method
				form.attr('action', '/page');
				method.attr('value', 'PUT');

				// Pull up the page info
				var page_id = $('#global_page_id').text();
				var page_title = $('#global_page_title').html().replace(/"/g, '&quot;');
				var page_long_title = $('#global_page_long_title').text();
				var page_description = $('#global_page_description').text();
				var page_route = $('#global_page_route').text();
				var disabled_options = $('#global_page_disabled').text();
				var page_visible = ($('#global_page_visible').text() == '1') ? 'checked' : '';

				disabled_options = disabled_options.split('|');

				// Create the disabled variables
				var route_disabled  = (disabled_options.indexOf('route') != -1) ? 'disabled' : '';
				var layout_disabled = (disabled_options.indexOf('layout') != -1) ? 'disabled' : '';
				var delete_disabled = (disabled_options.indexOf('delete') != -1) ? 'disabled' : '';

				// Build the panel content
				// This is row one
				content_row_one = '<div class="fourcol">';
				content_row_one    +=     '<input type="hidden" name="page_id" value="' + page_id + '" />';
				content_row_one    +=     '<label class="fl-label" for="page_title">Page Title <abbr class="icon-info-circled" title="This is the text that will be used for links that point to this page in the navigation areas of your site."></abbr></label>';
				content_row_one    +=     '<input type="text" class="fl-text" id="page_title" name="page_title" value="' + page_title + '" placeholder="Enter a title..." />';
				content_row_one    +=     '<label class="fl-label" for="page_route">Page Address</label>';
				content_row_one    +=     '<div class="fl-panel-route">';
				content_row_one    +=         '<p class="fl-panel-route-base">http://www.fletch.dev/</p>';
				content_row_one    +=         '<input type="text" class="fl-text fl-panel-route-href" id="page_route" name="page_route" value="' + page_route + '" placeholder="yourpage" ' + route_disabled + ' />';
				content_row_one    +=     '</div>';
				content_row_one    += '</div>';

				// This is row three
				content_row_two     = '<div class="fourcol">';
				content_row_two    +=     '<label class="fl-label" for="page_description">Page Description (optional) <abbr class="icon-info-circled" title="The will be displayed beneath search results on Google or other search engines."></abbr></label>';
				content_row_two    +=     '<textarea class="fl-textarea" name="page_description" placeholder="Enter a description...">' + page_description + '</textarea>';
				content_row_two    += '</div>';

				// This is row two
				content_row_three     = '<div class="fourcol last">';
				content_row_three    +=     '<label class="fl-label" for="page_long_title">Descriptive Page Title (optional) <abbr class="icon-info-circled" title="This is the text that will be used for the title of your page in search results. If none is entered, the page title will be used in its place."></abbr></label>';
				content_row_three    +=     '<input type="text" class="fl-text" id="page_long_title" name="page_long_title"  value="' + page_long_title + '" placeholder="Enter a title..." />';
				content_row_three    +=		'<div class="fl-panel-halfcol">';
				content_row_three    +=     	'<label class="fl-label">Page Visibility</label>';
				content_row_three    +=			'<div class="fl-panel-visibility">';
				content_row_three    +=        	'<input type="checkbox" name="page_visible" class="fl-panel-visibility-checkbox" id="page_visible" ' + page_visible + ' value="1">';
				content_row_three    +=        		'<label class="fl-panel-visibility-label" for="page_visible">';
				content_row_three    +=          		'<div class="fl-panel-visibility-inner"></div>';
				content_row_three    +=          	 	'<div class="fl-panel-visibility-switch"></div>';
				content_row_three    +=         	'</label>';
				content_row_three    +=     	'</div>';
				content_row_three    +=		'</div>';	
				content_row_three    +=		'<div class="fl-panel-halfcol">';
				content_row_three    +=     	'<label class="fl-label" for="page_delete">Delete Page <abbr class="icon-info-circled" title="In order to delete this page, you must type delete in this box and click on Save Changes"></abbr></label>';
				content_row_three    +=     	'<input type="text" class="fl-text" id="page_delete" name="page_delete" placeholder="Type &quot;Delete&quot;" ' + delete_disabled + ' />';
				content_row_three    +=		'</div>';	
				content_row_three    += '</div>';



				// This is a divider
				divider = "</div></div><div class='row'>";

				// This is row four
				content_row_four = '<div class="twelvecol last fl-layouts" style="margin-top: 1em;">';
				content_row_four    +=     '<label class="fl-label" style="margin-bottom: 1em">Choose a Page Layout</label>';				
				content_row_four    +=     '<input type="radio" name="layout" value="twocol_single" id="twocol_single" class="fl-layout fl-twocol-single"' + layout_disabled + ' />';
				content_row_four    +=     '<label class="fl-label" for="twocol_single"> </label>';
				content_row_four    +=     '<input type="radio" name="layout" value="twocol_double" id="twocol_double" class="fl-layout fl-twocol-double"' + layout_disabled + ' />';
				content_row_four    +=     '<label class="fl-label" for="twocol_double"> </label>';
				content_row_four    +=     '<input type="radio" name="layout" value="twocol_triple" id="twocol_triple" class="fl-layout fl-twocol-triple"' + layout_disabled + ' />';
				content_row_four    +=     '<label class="fl-label" for="twocol_triple"> </label>';
				content_row_four    += '</div>';


				// Display all three rows
				content.html(content_row_one + content_row_two + content_row_three + divider + content_row_four);

				// Select the appropriate layout
				layout = $('#global_page_layout').text();

				console.log(layout);
				$('.fl-layouts > input#' + layout).attr("checked","checked");

			break;

			case 'manage_social_media' :
				// Set the settings panel title
				title.text('Manage Social Media');
				panel.css('height', '28em');

				var facebook = $('#setting_facebook').text();
				var twitter = $('#setting_twitter').text();
				var linkedin = $('#setting_linkedin').text();
				var googleplus = $('#setting_googleplus').text();
				var instagram = $('#setting_instagram').text();
				var youtube = $('#setting_youtube').text();
				var pinterest = $('#setting_pinterest').text();

				// Set the form action and method
				form.attr('action', '/settings?type=socialmedia');
				method.attr('value', 'PUT');

				// Build the panel content
				// This is row one
				content_row_one = '<div class="fourcol">';
				content_row_one    +=     '<label class="fl-label" for="facebook">Facebook</label>';
				content_row_one    +=     '<input type="text" class="fl-text" id="facebook" name="facebook" value="' + facebook + '" />';
				content_row_one    +=     '<label class="fl-label" for="twitter">Twitter</label>';
				content_row_one    +=     '<input type="text" class="fl-text" id="twitter" name="twitter" value="' + twitter + '" />';
				content_row_one    +=     '<label class="fl-label" for="linkedin">LinkedIn</label>';
				content_row_one    +=     '<input type="text" class="fl-text" id="linkedin" name="linkedin" value="' + linkedin + '" />';
				content_row_one    += '</div>';

				// This is row two
				content_row_two = '<div class="fourcol">';
				content_row_two    +=     '<label class="fl-label" for="googleplus">Google+</label>';
				content_row_two    +=     '<input type="text" class="fl-text" id="googleplus" name="googleplus" value="' + googleplus + '" />';
				content_row_two    +=     '<label class="fl-label" for="instagram">Instagram</label>';
				content_row_two    +=     '<input type="text" class="fl-text" id="instagram" name="instagram" value="' + instagram + '" />';
				content_row_two    += '</div>';

				// This is row three
				content_row_three = '<div class="fourcol last">';
				content_row_three    +=     '<label class="fl-label" for="youtube">Youtube</label>';
				content_row_three    +=     '<input type="text" class="fl-text" id="youtube" name="youtube" value="' + youtube + '" />';
				content_row_three    +=     '<label class="fl-label" for="pinterest">Pinterest</label>';
				content_row_three    +=     '<input type="text" class="fl-text" id="pinterest" name="pinterest" value="' + pinterest + '" />';
				content_row_three    += '</div>';

				// Display all three rows
				content.html(content_row_one + content_row_two + content_row_three);
			break;


			case 'create_new_page' :
				// Set the settings panel title
				title.text('Create a New Page');

				// Set the form action and method
				form.attr('action', '/page');
				method.attr('value', 'POST');
				panel.css('height', '37em');


				// Build the panel content
				// This is row one
				content_row_one = '<div class="fourcol">';
				content_row_one    +=     '<label class="fl-label" for="page_title">Page Title <abbr class="icon-info-circled" title="This is the text that will be used for links that point to this page in the navigation areas of your site."></abbr></label>';
				content_row_one    +=     '<input type="text" class="fl-text" id="page_title" name="page_title" placeholder="Enter a title..." />';
				content_row_one    +=     '<label class="fl-label" for="page_route">Page Address</label>';
				content_row_one    +=     '<div class="fl-panel-route">';
				content_row_one    +=         '<p class="fl-panel-route-base">http://www.fletch.dev/</p>';
				content_row_one    +=         '<input type="text" class="fl-text fl-panel-route-href" id="page_route" name="page_route" placeholder="yourpage" />';
				content_row_one    +=     '</div>';
				content_row_one    += '</div>';

				// This is row three
				content_row_two = '<div class="fourcol">';
				content_row_two    +=     '<label class="fl-label" for="page_description">Page Description (optional) <abbr class="icon-info-circled" title="The will be displayed beneath search results on Google or other search engines."></abbr></label>';
				content_row_two    +=     '<textarea class="fl-textarea" name="page_description" placeholder="Enter a description..."></textarea>';
				content_row_two    += '</div>';

				// This is row two
				content_row_three = '<div class="fourcol last">';
				content_row_three    +=     '<label class="fl-label" for="page_long_title">Descriptive Page Title (optional) <abbr class="icon-info-circled" title="This is the text that will be used for the title of your page in search results. If none is entered, the page title will be used in its place."></abbr></label>';
				content_row_three    +=     '<input type="text" class="fl-text" id="page_long_title" name="page_long_title" placeholder="Enter a title..." />';
				content_row_three    += '</div>';

				// This is a divider
				divider = "</div></div><div class='row'>";

				// This is row four
				content_row_four = '<div class="twelvecol last fl-layouts" style="margin-top: 1em;">';
				content_row_four    +=     '<label class="fl-label" style="margin-bottom: 1em">Choose a Page Layout</label>';	
				content_row_four    +=     '<input type="radio" name="layout" value="twocol_single" id="twocol_single" class="fl-layout fl-twocol-single" checked/>';
				content_row_four    +=     '<label class="fl-label" for="twocol_single"> </label>';
				content_row_four    +=     '<input type="radio" name="layout" value="twocol_double" id="twocol_double" class="fl-layout fl-twocol-double" />';
				content_row_four    +=     '<label class="fl-label" for="twocol_double"> </label>';
				content_row_four    +=     '<input type="radio" name="layout" value="twocol_triple" id="twocol_triple" class="fl-layout fl-twocol-triple" />';
				content_row_four    +=     '<label class="fl-label" for="twocol_triple"> </label>';
				content_row_four    += '</div>';

				// Display all three rows
				content.html(content_row_one + content_row_two + content_row_three + divider + content_row_four);
			break;
		}

		$('#matte').fadeTo(150, 0.35);

		// The first part "preps" the animation so that
		// it doesn't have so much trouble with lag.
		$('#settings_panel').animate({
    		height: '-=1'
		}, 1, 'linear').animate({
			opacity: 'show',
			height: 'show'
			}, 250, function() {
				// Animation complete.
			});
	},


	// Hide the settings panel.
	hideSettings: function() {

		$('#matte').fadeOut(150);
		$('body').css('overflow', 'auto');

		// The first part "preps" the animation so that
		// it doesn't have so much trouble with lag.
		$('#settings_panel').animate({
    		height: '-=1'
		}, 1, 'linear').animate({
			opacity: 'hide',
			height: 'hide'
			}, 250, function() {
				// Animation complete.
			});
	},

	// Save the options in the settings panel
	saveSettings: function(e) {
		
		var error_message = '';

		// Clear the setting alert so that we can start
		// fresh with any alerts.
		Ui.clearSettingAlert();

		// Detect which type of settings panel we have open so that we can appropriately validate
		var type = $('#settings_panel').attr('data-type');

		switch(type) {
			case 'manage_current_page':
			case 'create_new_page':

				var title = $('#settings_panel').find('#page_title').val();
				var route = $('#settings_panel').find('#page_route').val();
				var current_route = $('#global_page_route').text();

				// If any one of these conditions fails, we cancel the request
				// and display an error message via JavaScript
				if(!Validate.route(route, current_route) || !Validate.hasText(title)) {
					e.preventDefault();
					error_message = (Validate.route(route, current_route)) ? error_message : 'You must enter a page address that is not already in use and contains only alphanumeric characters, underscores, and dashes, and does not end or begin with an underscore or dash.' ;
					error_message = (Validate.hasText(title)) ? error_message : 'You must enter a title.' ;
					Ui.showSettingAlert(error_message);
				}
				break;

			case 'manage_social_media':
				// No validation currently needed
				break;
		}	
	},
};
/*!
 * EditableImage Class
 * Written by Pete Molinero of Laterna Studio
 * http://www.laternastudio.com
 * All Rights Reserved.
 *
 * Synopsis:
 * Instances of EditableImage will be created and used. As such, NONE of the methods are static.
 */

var editableImages = [];

function EditableImage(id) 
{
	this.id = id;
	this.old = '';
	this.element_id = $('#' + id).closest('li').attr('data-element-id');
    this.select_id = "#select_" + id;
    this.content_id = $('#' + id).attr('data-url-id');
	this.action = '/elements/' + this.element_id + '/' + this.content_id; // The url that will process image data
    this.div_id = "#div_" + id;
    this.pending_id = "#pending_" + id;
    this.aspect_ratio = 0;
    this.size = 'any';
    this.lightbox = 'true';

    // Initialize a new editable image
    this.init = function() {
        this.construct();
        this.listen();
    },

	this.construct = function()
	{
		var image_h  = $('#' + this.id).height(); // The height of the image
		var image_w  = $('#' + this.id).width(); // The width of the image
		var ribbon_h  = $('.container').height(); // The height of the image

		// Remove the border from the image so that it can be added by the div instead
		$('#' + this.id).removeClass('beauty');
		$('#' + this.id).css('width', '100%');

		// Create a div that encapsulates the image. This allows the file
		// select button to be overlayed and "clipped".
		$('#' + this.id).wrap('<div style="height: '+image_h+'px;" class="fl-editable-image-container beauty-div" id="div_' + this.id + '" ></div>');

		// This is the HTML for the form that the editable images rely on.
		// Basically it is prepended into the image span, then set at a
		// low opacity so that when a user clicks on the image, they are
		// Unknowingly clicking on this box.
        var form_html  = "<form action='" + this.action + "' method='post' enctype='multipart/form-data' id='form_" + this.id + "' >";
        form_html     += "<input type='file' name='image' id='select_" + this.id + "' ";
        form_html     += "style='height: " + image_h + "px;' class='fl-editable-image-file-select' title='Click to change.' />";
        form_html     += "<input type='hidden' name='_method' value='PUT' /></form>";
		
		// Append the form and file select box
		$('#div_' + this.id).append(form_html);

        // Set various image attributes
        this.setAspectRatio();
        this.setSize();
        this.setLightbox();
	},

	this.enableHover = function() {
		// Create a hover effect on the image
        $('#select_' + this.id).hover(
            function () {
                if(!Reposition.dragging) {
                    var image_id = '#'+$(this).attr('id').substr(7);
                    $(image_id).animate({
                        opacity: 0.55
                    }, 100);
                }
            },
            function () {
                var image_id = '#'+$(this).attr('id').substr(7);
                $(image_id).animate({
                    opacity: 1
                }, 100);
            }
		);
	},

    // Remove the hover effect from the image
	this.disableHover = function() {
		$('#select_' + this.id).unbind('mouseenter mouseleave');
	},


	this.listen = function() {

		this.enableHover();

       // Define the url for the image editability
        var options = {
            url: this.action
        }; 
         
        // Make the ajax form active
        $("#form_" + this.id).ajaxForm(options);

        // Set up the AJAX portion (update the image preview when an image is selected)
        $("#select_" + this.id).change(function (e){

            var id,
                filename;

            // Grab the filename/path from the file dialog input
            filename = $(this).val();

            // Get the id for the editable image object
            id = $(this).attr('id').substr(7);
            
            // Get the image object and execute the change
            imageObj = editableImages[id];
            imageObj.changeTo(filename);

            return false; // avoid to execute the actual submit of the form.
        });

    },

    this.setAspectRatio = function() {

        var div,
            aspect_ratio;

        // Grab the div that has the options on it
        div = $(this.div_id).parent();

        // Find out what the aspect ratio should be
        aspect_ratio = (div.attr('data-aspect-ratio')) ? div.attr('data-aspect-ratio') : 0;

        // Set the aspect ratio of the object
        this.aspect_ratio = aspect_ratio;
    },

    this.setSize = function() {

        var div,
            size;

        // Grab the div that has the options on it
        div = $(this.div_id).parent();

        size = (div.attr('data-size')) ? div.attr('data-size') : 'any';

        this.size = size;
    },
        
    this.setLightbox = function() {

        var div,
            lightbox;

        // Grab the div that has the options on it
        div = $(this.div_id).parent();

        lightbox = (div.attr('data-lightbox')) ? div.attr('data-lightbox') : 'false';

        this.lightbox = lightbox;
    },

    this.addProgressSymbol = function() {
        // Creat the spinning loading symbol
        div = $(this.div_id);
        div.append('<span id="' + this.pending_id.substring(1) + '" class="icon-spin3 animate-spin fl-editable-image-loading"></span>');

        // Select the appended symbol
        upload_icon = $(this.pending_id); 

        // Position the spinning symbol in the center of the image
        coords = Utility.centerCoords(div, upload_icon);

        upload_icon.css('left', coords.left);
        upload_icon.css('top', coords.top);

        // Fade out the original image a little bit
        this.disableHover();
        $(this.div_id + ' img').fadeTo('slow', 0.5);
    },

    this.removeProgressSymbol = function() {
        // Select the appended symbol
        upload_icon = $(this.pending_id);

        // Remove it!
        upload_icon.remove();
    },

    // Change an image
    this.changeTo = function(filename) {

        var options,
            imageObj,
            success;
        
        // So that we don't get an error when the user clicks cancel.
        if(filename == '') return false;

        // Do a javascript file type validation
        if(!Validate.filetype(filename)) {
            Utility.alert("You must select a JPG, GIF or PNG image file.", 'Invalid File Type');
            return false;
        }

        // Get the id for the editable image object
        imageObj = this;

        // Add the progress symbol (spinning arrows)
        imageObj.addProgressSymbol();
        
        // Update the image
        Database.update_content(imageObj.element_id, imageObj.content_id, 'image', {image_object: imageObj});
        
    },

    this.completeUpload = function(data) {

        // We need to do this in order to pass 'this' into the load object
        var that = this;

        // Remove the spinning icon
        that.removeProgressSymbol();

        // Show the publish and undo buttons
        Ui.showPublishPanel();

        filename = data.url;

        if(this.lightbox == "true") {
            extension = filename.split('.').pop();
            thumb_filename = filename.substring(0, filename.length - 4) + '_thumb.' + extension;
            filename = thumb_filename;
        }

        // Replace the old image with the new image
        $('#' + that.id).replaceWith('<img class="editableImage" src="' + filename + '" id="' + that.id + '" data-url-id="' + that.content_id + '"style="width: 100%;" />');
        
        // When the new image loades up, recalculate the size and re-enable the hover effect
        $('#' + that.id).load(function(){
            that.resize();
            that.enableHover();
        });
    },

    this.failUpload = function() {
        // Remove the progress icon
        this.removeProgressSymbol();

        // Enable the hover again
        this.enableHover();

        // Return the opacity to 1.0
        $('#div_' + this.id + ' img').fadeTo('fast', 1);
    },

    this.resize = function() {

        var new_height = $('#' + this.id).height();
        $('#div_' + this.id).height(new_height);
        $('#select_' + this.id).height(new_height);

    }

};
/*!
 * Validate
 * Written by Pete Molinero of Laterna Studio
 * http://www.laternastudio.com
 * All Rights Reserved.
 *
 * Synopsis:
 * A class for validating data.
 */

var Validate = {

	// Validate a legal file type
	filetype: function(filename) {

		// Default to illegal
		legal = false;

		// If any of these extensions are found in the filename, it's a legal filetype
		if(filename.indexOf("jpg") !== -1 ||
            filename.indexOf("jpeg") !== -1 ||
            filename.indexOf("gif") !== -1 ||
            filename.indexOf("png") !== -1 ||
            filename.indexOf("JPG") !== -1 ||
            filename.indexOf("JPEG") !== -1 ||
            filename.indexOf("GIF") !== -1 ||
            filename.indexOf("PNG") !== -1
        ) {
			legal = true;
        }

        return legal;
	},

	// Validates that a string isn't empty
	hasText: function(str) {

		// Check to see if the string is empty, null, or undefined
		var hasText = (str !== '' && str !== null && str !== undefined);

		// Return the boolean result of the check
		return hasText;
	},

	// Validate a route (such as from the settings panel)
	route: function(route, exempt) {

		var allowed_route,
			route_unique,
			route_okay,
			title_okay,
			error_message;

		// Must start and end with letters and numbers, and may
		// only have dashes, underscores, and forward slashes inbetween
		allowed_route = /^[a-z0-9]+[a-z0-9\/\-\_]+[a-z0-9]$/i;

		// The route must NOT be in the reserved_routes array
		// the +1 is because if inArray returns a 0 it will be considered false
		route_unique = ($.inArray(route, reserved_routes) + 1 && route != exempt) ? false : true;

		// If the route is the home-page route it is okay.
		// Otherwise, the route must pass the allowed_route test
		if(route == '/') {
			route_okay = true;
		}
		else {
			route_okay = allowed_route.test(route);
		}
		
		return (route_okay && route_unique);
	},

	// Ensure that a url does, in fact, include a youtube code
	youtube: function(url) {
		var valid_youtube_code = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
		console.log(url);
		console.log(valid_youtube_code.test(url));
		return valid_youtube_code.test(url);
	}
};
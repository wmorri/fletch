$(function() {

	// Begin the mobile navigation
	MobileNav.init();

	$('#email_list_submit').on('click', function(e) {
		e.preventDefault();

		var email = $('#email_list_value').val();
		$('#email_list_submit').attr('disabled', true);

		$.post("/settings?type=emaillist", { "_method": "PUT", email: email },
		  function(data){
		  	if(data.success == 'true') {
		  		$('.subscribe').replaceWith('<p style="color: green;">Thankyou for your e-mail!</p>');
		  	}
		  	else {
		  		$('#email_list_submit').attr('disabled', false);
				$('#email_list_value').css('border-color', 'red');
				$('#email_list_value').css('background-color', 'pink');
		  	}
		  }, "json");
	});

	$('#inline_email_list_submit').on('click', function(e) {
		e.preventDefault();

		var email = $('#inline_email_list_value').val();
		$('#inline_email_list_submit').attr('disabled', true);

		$.post("/settings?type=emaillist", { "_method": "PUT", email: email },
		  function(data){
		  	if(data.success == 'true') {
		  		$('#inline_subscribe').replaceWith('<p style="color: green;">Thankyou for your e-mail!</p>');
		  	}
		  	else {
		  		$('#inline_email_list_submit').attr('disabled', false);
				$('#inline_email_list_value').css('border-color', 'red');
				$('#inline_email_list_value').css('background-color', 'pink');
		  	}
		  }, "json");
	});


});


